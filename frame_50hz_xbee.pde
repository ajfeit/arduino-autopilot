//#include <avr/interrupt.h>  
//#include <avr/io.h>  
//#include <Wire.h>
#include <Servo.h>
#include <umfpuV3.h>
#include <EEPROM.h>
  
//#define INIT_TIMER_COUNT 0  
//#define RESET_TIMER_II TCNT2 = INIT_TIMER_COUNT
#define YAW_PIN 0
#define PITCH_PIN 1
#define ZACC_PIN 2
#define YACC_PIN 3
#define XACC_PIN 6
#define DATAOUT 11//MOSI
#define DATAIN 12//MISO - not used, but part of builtin SPI
#define SPICLOCK  13//sck
#define SLAVESELECT 10//ss
#define FPU_R 8
#define GPS_R 7
#define DTIME 0.0100480
#define BASE_DATA_BEGIN 1
#define ENDPOINT_ADJUST 2

//setup servos for output
Servo servo1;
Servo servo2;
Servo servo3;

//for servos
volatile int output1 = 90;
volatile int output2 = 90;
volatile int output3 = 90;
int val, axis;
volatile int input_data[5];
volatile int control_input[5];
volatile boolean button1, button2, button3, button4;
 
volatile boolean ledState = LOW;
volatile float yaw_rate = 0;
volatile float pitch_rate = 0;
volatile float pitch_rate_hf = 0;
volatile float pitch_rate_hf_last = 0;
volatile float yaw_rate_hf = 0;
volatile float yaw_rate_hf_last = 0;
volatile float pitch = 0;
volatile float yaw = 0;
volatile float xacc;
volatile float yacc;
volatile float zacc;
volatile int yaw_bias = 0;
volatile int pitch_bias = 0;
volatile int yacc_bias = 0;
volatile int xacc_bias = 0;
volatile int zacc_bias = 0;
volatile boolean servo_refresh;
volatile boolean initialize = HIGH;
volatile boolean base_data_ready = LOW;
volatile boolean endpoint_adj = LOW;
volatile float g = 9.81;
volatile float tanpitch;
volatile float pitch_lf = 0;
volatile float pitch_out;
volatile float pitch_temp;
volatile float pitch_rate_lf;
volatile float pitch_rate_lf_last;
int axis1_min_pulse, axis2_min_pulse, axis3_minpulse, axis1_max_pulse, axis2_max_pulse; 
char b[22];

void calibrate_imu();

//100.16025641 Hz or once every 156*1024 = 159744 clock cycles 
ISR(TIMER2_COMPA_vect) {  
  //digitalWrite(ledPin, HIGH);
  //read serial control inputs
  
   while (Serial.available()) {
    val = Serial.read();
    if (val == 0) { axis = 0;}
    else
    {
      axis += 1;
      if (axis == 1) {
        if (val == 1) { endpoint_adj = HIGH;}      
        if (val == 2) { base_data_ready = HIGH;}
      }
      else {
        if (endpoint_adj || base_data_ready) {input_data[axis] = val;}
        else {
          control_input[axis] = val;
          if (axis == 4) {
            val -= 1;
            button1 = ((val & 1) == 1);
            button2 = ((val & 2) == 1);
            button3 = ((val & 4) == 1);
            button4 = ((val & 8) == 1);
          }
        }
      }
     }
   }
   
  //digitalWrite(ledPin, button1);
  
  //read and scale IMU inputs
  
  //1024 mu/5v  4.88 mV ber mu,  15mV/deg/sec
  yaw_rate = constrain(((float)(analogRead(YAW_PIN) - yaw_bias))/3.0737705, -100, 100);

 //5mv/deg/sec  
  pitch_rate = constrain(((float)(analogRead(PITCH_PIN) - pitch_bias))*.976, -300, 300);
  
  //1000 mV per g, 1024 mu/5v,  
  xacc = ((float)(analogRead(XACC_PIN) - xacc_bias))*g/204.8;     
  yacc = ((float)(analogRead(YACC_PIN) - yacc_bias))*g/204.8;
  zacc = (((float)(analogRead(ZACC_PIN) - 512))*g/204.8);
  
  if (initialize == HIGH) 
  {
  pitch = 0;
  yaw = 0;
  yaw_rate_hf = yaw_rate;
  yaw_rate_hf_last = yaw_rate;
  pitch_rate_lf = 0;
  pitch_rate_lf_last = 0;
  pitch_temp = 0;
  }
  //calculate steady state angle based on acc.
  if (xacc == 0) {
    if (zacc == 0) {
      pitch_lf = 0;
    }
    else if (zacc > 0) { pitch_lf = 180;}
    else if (zacc < 0) { pitch_lf = 0;}
  }
  else if (zacc == 0) {
    if (xacc > 0) { pitch_lf = -90;}
    else if (xacc < 0) { pitch_lf = 90;}
  }
  else {
    tanpitch = xacc/zacc;
    if (zacc < 0) { 
      if (xacc < 0) {
      pitch_lf = arctan(tanpitch);
      }
      else if (xacc > 0) {
        pitch_lf = -arctan(-tanpitch);
      }
    }
    else if (zacc > 0) {
      if (xacc < 0) {
      pitch_lf = 180 - arctan(-tanpitch);
      }
      else if (xacc > 0) {
        pitch_lf = -180 + arctan(tanpitch);
      }
    }
  } 
  
  //low pass filter the pitch calculated from the accelerometers
  //lp filter lf_pitch
  //pitch_temp = pitch_lf;
  pitch_temp += ((pitch_rate_lf + pitch_rate_lf_last)*DTIME/2.0);
  pitch_rate_lf_last = pitch_rate_lf;
  pitch_rate_lf = pitch_lf - pitch_temp; // 1 rad/sec
  if (pitch_rate_lf >= 180) {pitch_rate_lf -= 360;}
  if (pitch_rate_lf < -180) {pitch_rate_lf += 360;}
  if (pitch_temp >= 180) {pitch_temp -= 360;}
  if (pitch_temp < -180) {pitch_temp += 360;}
  
  //hp filter pitch rate and integrate pitch_rate to get attitude
  pitch += ((pitch_rate_hf + pitch_rate_hf_last)*DTIME/2.0);
  pitch_rate_hf_last = pitch_rate_hf;
  pitch_rate_hf = pitch_rate - pitch;    //bandwidth of 1 rad/sec
  if (pitch > 180) { pitch -= 360;}
  if (pitch <= -180) { pitch += 360; }
  
  
  //hp filter yaw rate and integrate yaw_rate to get heading
  yaw += ((yaw_rate_hf + yaw_rate_hf_last)*DTIME/2.0);
  yaw_rate_hf_last = yaw_rate_hf;
  yaw_rate_hf = yaw_rate - yaw;
  if (yaw_rate_hf >= 180) {yaw_rate_hf -= 360;}
  if (yaw_rate_hf < -180) {yaw_rate_hf += 360;}
  if (yaw > 180) { yaw -= 360;}
  if (yaw <= -180) { yaw += 360; }
   
  pitch_out = pitch + pitch_temp;
     
  //calculate control outputs
  output3 = input_data[2]-82;  //throttle to speed control
  output2 = input_data[0]-82;  //elevator
  output1 = input_data[1]-82;  //rudder

  //update servo positions
  servo1.write(output1);
  servo2.write(output2);
  servo3.write(output3);
  servo1.refresh();
  servo2.refresh();
  servo3.refresh();
  
  //no longer on first time through
  initialize = LOW; 
};  
   
void setup() {  
  //pinMode(ledPin, OUTPUT);
  //pinMode(3, OUTPUT);
  Serial.begin(57600);  
  
  //Setup SPI interface with FPU
  pinMode(FPU_R, INPUT);
  pinMode(GPS_R, INPUT);
  byte clr;
  pinMode(DATAOUT, OUTPUT);
  pinMode(DATAIN, INPUT);
  pinMode(SPICLOCK,OUTPUT);
  pinMode(SLAVESELECT,OUTPUT);
  digitalWrite(SLAVESELECT,HIGH); //disable device
  /* Enable SPI, Master, set clock rate fck/16 */
  SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR0);
  clr=SPSR;
  clr=SPDR;
  delay(10);
  reset_fpu();
  delay(50);
  
  //Timer2 Settings: Timer Prescaler /1024,   
  TCCR2B |= (1 << CS22) | (1 << CS21) | (1 << CS20);      
  //TCCR2B &= ~(1<<CS20);       
  // Use normal mode  
  //TCCR2A &= ~((1<<WGM21) | (1<<WGM20));
  // Use CTC Mode to adjust frame timing
  TCCR2A &= ~(1<<WGM20);
  TCCR2A |= (1<<WGM21);
  // Use internal clock - external clock not used in Arduino  
  ASSR |= (0<<AS2);  
  //output compare regsiter A for timer 2, comapre to 156
  OCR2A = 156;
  //Timer2 Overflow Interrupt Enable  
  //TIMSK |= (1<<TOIE2) | (0<<OCIE2); 
  //Timer2 output compare interrupt enable  
  TIMSK2 |= (1<<OCIE2A);
  //TIMSK2 &= ((0<<TOIE2) && (0<<OCIE2B))
  
  //RESET_TIMER_II;
  TCNT2 = 0;   //INIT_TIMER_COUNT  
  sei();  
  //starttime = millis();
   
  //Setup servo #1, adjust endpoints
  servo1.attach(2);
  servo1.setMinimumPulse(800);
  servo1.setMaximumPulse(2200);
  
  //Setup servo #2, adjust endpoints
  servo2.attach(3);
  servo2.setMinimumPulse(900);
  servo2.setMaximumPulse(2200);
  
  //Setup servo #3, adjust endpoints
  servo3.attach(4);
  servo2.setMinimumPulse(900);
  servo2.setMaximumPulse(2200);
  
  calibrate_imu();
  delay(10);
  while (!digitalRead(FPU_R)) { }
  //tell FPU to stary receiving GPS
  spi_transfer(FCALL);
  spi_transfer(4);
  delay(10);
}  
     
void loop() {
  //communicate with FPU and send data to base station
  //delay so that serial buffers don't overflow
  delay(20);
  //send pitch angle
  Serial.print(pitch_out, DEC);
  Serial.print(",");
  Serial.print(yaw_rate, DEC);
  Serial.print(",");
  
  //Get FPU data, and send data to base station
  //Check to see if FPU is ready
  if (digitalRead(FPU_R) && !(digitalRead(GPS_R)))
  {
    //check if a sentence has been recieved
    spi_transfer(FCALL);
    spi_transfer(5);
    delay(10);
  }
  if (digitalRead(GPS_R) && digitalRead(FPU_R)){
      //Process GPS Sentence
      spi_transfer(FCALL);
      spi_transfer(6);
      delay(10);
      //Read the data 
      // first read validity, and satellites used, both are just one byte
       //read validity
       spi_transfer(SELECTA);
       spi_transfer(22);
       spi_transfer(LREADBYTE);
       delay(1);
       b[0] = spi_transfer(0);
       Serial.print((byte) b[0], DEC);
       Serial.print(",");
       
       //read satellites used
       spi_transfer(SELECTA);
       spi_transfer(28);
       spi_transfer(LREADBYTE);
       delay(1);
       b[1] = spi_transfer(0);
       Serial.print((byte) b[1], DEC);
       Serial.print(",");
       if (b[0] == 2) 
       {      
         //read the latitude, longitude, 
         //read lat in degrees, 4 bytes
         spi_transfer(SELECTX);
         spi_transfer(0x0A);
         spi_transfer(RDBLK);
         spi_transfer(5);
         delay(1);
         for (int i = 2;i <= 21;i++)
         {
           b[i] = spi_transfer(0);
           Serial.print((byte) b[i], DEC);
           Serial.print(",");
         }
      }
      spi_transfer(FCALL);
      spi_transfer(5);
      delay(2);
    }
    Serial.println();
}

void calibrate_imu()
{
  //read standing still imu values to find zero
  for (int i = 0;i <= 9;i++)
  {
  yaw_bias += analogRead(YAW_PIN);
  pitch_bias += analogRead(PITCH_PIN);
  yacc_bias += analogRead(YACC_PIN);
  xacc_bias += analogRead(XACC_PIN);
  //zacc_bias += analogRead(ZACC_PIN);
  //g += sqrt((float)yacc_bias*yacc_bias + (float)xacc_bias*xacc_bias);
  delay(20);
  }
  yaw_bias /= 10;
  pitch_bias /= 10;
  yacc_bias /= 10;
  xacc_bias /= 10;
  zacc_bias /= 10;
  //g /= 10;
}

float arctan(float tangent)
{
  if (tangent > 1 || tangent < -1) {
    tangent = 1/tangent;
    return(90 - (-0.555 + tangent*(55.93 + tangent*(0.324 - 11.46*tangent))));
  }
  else {
    return(-0.555 + tangent*(55.93 + tangent*(0.324 - 11.46*tangent)));
  }  
}

char spi_transfer(volatile char data)
{
  SPDR = data;                    // Start the transmission
  while (!(SPSR & (1<<SPIF)))     // Wait the end of the transmission
  {
  };
  return SPDR;                    // return the received byte
}

void reset_fpu()
{
  
  spi_transfer(0xFF);
  spi_transfer(0xFF);
  spi_transfer(0xFF);
  spi_transfer(0xFF);
  spi_transfer(0xFF);
  spi_transfer(0xFF);
  spi_transfer(0xFF);
  spi_transfer(0xFF);
  spi_transfer(0xFF);
  spi_transfer(0xFF);
  spi_transfer(0);
  delay(10);
}
